#!/bin/bash

CONTENTPATH=lektor
OUTPUTPATH=../htdocs

IMAGENAME=edgesoft/lektor
CONTAINERNAME=lektor

echo "Run lektor server"
echo

sudo docker run --rm --name $CONTAINERNAME --env LEKTOR_OUTPUT_PATH=$OUTPUTPATH --volume $(pwd):/opt/lektor --publish 5000:5000 $IMAGENAME --project $CONTENTPATH server --host 0.0.0.0
