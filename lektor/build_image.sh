#!/bin/bash

IMAGENAME=edgesoft/lektor

echo "Build $IMAGENAME image"
echo

sudo docker build --tag $IMAGENAME .
