#!/bin/bash

echo "Removes all stopped containers"
echo

sudo docker rm $(sudo docker ps -a -q)
